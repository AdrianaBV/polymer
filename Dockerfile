# Imagen base
FROM node:latest

# Directorio de la app en el contenedor
WORKDIR /app

# Copiado de archivos
ADD build/default /app/build/default
ADD package.json /app
ADD server.js /app

# Dependencias
RUN npm install

# Puerto que expongo
EXPOSE 3000

# Comando para ejecutar el servicio
CMD ["npm", "start"]
