//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyparser = require('body-parser');
app.use(bodyparser.json());

var path = require('path');

var cors = require('cors');
app.use(cors())

app.use(express.static(__dirname + "/build/default"));

app.listen(port);

console.log('Proyecto Polymer con wrapper de NodeJS on: ' + port);

app.get('/', function(req, res) {
  //res.send("Hola mundo desde NodeJS");
  res.sendFile('index.html',{root:'.'});
})
